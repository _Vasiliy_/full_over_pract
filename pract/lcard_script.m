clc;
clear;



E = 24;
K = 1.077554370127206;
harm_number = 5;
PWM_freq = 14000;
freqs = zeros(1,harm_number);
for i=1:harm_number
    freqs(i) = i*PWM_freq;
end
% f1 = 14000;
% f2 = 2*f1;
% f3 = 3*f1;
% f4 = 4*f1;
% f5 = 5*f1;

AdcRate               = 3333333.333333; % ������� ��� � ��
RealChannelsQuantity  = 3;          % ����� ��������� (��������) �������
RealKadrsQuantity     = 2048;        % ����� ��������� ������ � ������� 'int' ( < 2.147.483.648)
RealSamplesQuantity   = 6144;        % ����� ��������� �������� � ������� 'int' (<2.147.483.648)
TotalTime             = 0.000614;
DataCalibrScale = [0.000405 0.000129 0.000405 ];        % ������ � ��������������� �����
DataCalibrOffset = [0.000000 0.000000 0.000000 ];        % ������ � ��������������� �������� ����
DataCalibrZeroK = [-13.630518 -19.302216 -21.379547 ];        %% ������ �� ��������� ����

freq_by_harm = AdcRate/RealKadrsQuantity;

fres1 = fopen('res_for_IL.m', 'w');
fres2 = fopen('res_for_Un.m', 'w');
fres3 = fopen('res_for_t.m', 'w');
fres4 = fopen('res_for_L.m', 'w');
fres5 = fopen('res_for_C_U.m', 'w');
fres6 = fopen('res_for_Rc_U.m', 'w');
    
for k = 10:10
    if (k == 100)
        DataFileName=strcat('C:\Users\BURBON\Documents\pract_nitki\lcard\ryad_0', num2str(k)); 
    end
    if (k > 9 && k<100)
        DataFileName=strcat('C:\Users\BURBON\Documents\pract_nitki\lcard\ryad_00', num2str(k));
    end
    if (k < 9)
        DataFileName=strcat('C:\Users\BURBON\Documents\pract_nitki\lcard\ryad_000', num2str(k));
    end

    fid = fopen(strcat(DataFileName,'_10om.dat'), 'r');
    fseek(fid, 0, -1);
    [y, NSamples] = fread(fid, 'int16');
    y = reshape(y, RealChannelsQuantity, RealKadrsQuantity);
    fclose(fid);

    t = linspace(0, TotalTime, RealKadrsQuantity);

    IL = (y(1,:)+DataCalibrZeroK(1))*DataCalibrScale(1)+DataCalibrOffset(1);
    In = (y(2,:)+DataCalibrZeroK(2))*DataCalibrScale(2)+DataCalibrOffset(2);
    Un = (y(3,:)+DataCalibrZeroK(3))*DataCalibrScale(3)+DataCalibrOffset(3);

    IL = IL * 1.633987 / K;
    In = (In / K) / 0.455;
    Un = (Un / K) / 0.252771100611434;

    ILF = medfilt1(IL,3);
    InF = medfilt1(In,3);
    UnF = medfilt1(Un,3);

    Ic = ILF - InF;
    FFT_Ic = fft(Ic);
    FFT_Un = fft(UnF);

    Um = zeros(1,harm_number);
    Im = zeros(1,harm_number);
    fi = zeros(1,harm_number);
    Z = zeros(1,harm_number);
    X = zeros(1,harm_number);
    R = zeros(1,harm_number);
    C = zeros(1,harm_number);
    C_resU = 0;
    Rc_resU = 0;
    C_resI = 0;
    Rc_resI = 0;
    harm_numbers_I = zeros(1,harm_number);
    harm_numbers_U = zeros(1,harm_number);

    for i=1:harm_number

        num_max_harmI = ceil(freqs(i)/freq_by_harm);     
        for j=num_max_harmI-2:num_max_harmI+2
            if (abs(FFT_Ic(j)) > abs(FFT_Ic(num_max_harmI)))
                num_max_harmI = j;
            end
        end
        harm_numbers_I(i) = num_max_harmI;

        num_max_harmU = num_max_harmI;
        for j=num_max_harmU-2:num_max_harmU+2
            if (abs(FFT_Un(j)) > abs(FFT_Un(num_max_harmU)))
                num_max_harmU = j;
            end
        end
        harm_numbers_U(i) = num_max_harmU;

        Im(i) = FFT_Ic(num_max_harmU);
        Um(i) = FFT_Un(num_max_harmI);

        fi(i) = angle(Um(i)) - angle(Im(i));
        while (abs(fi(i)) > pi)
            fi(i) = abs(fi(i)) - 2*pi;
        end
        if fi(i) < -0.1 && fi(i) > -pi/2
            Z(i) = (Um(i)/Im(i))*exp(1i*(fi(i)));
            X(i) = abs(Z(i)*sin(fi(i)));
            R(i) = abs(Z(i)*cos(fi(i)));
            C(i) = 1/(2*pi*freqs(i)*X(i)); 

            C_resU = C_resU + C(i)*abs(Um(i))/abs(Um(1));
            C_resI = C_resI + C(i)*abs(Im(i))/abs(Im(1));
            Rc_resU = Rc_resU + 1/R(i)*abs(Um(i))/abs(Um(1));
            Rc_resI = Rc_resI + 1/R(i)*abs(Im(i))/abs(Im(1));
        end

    end
    Rc_resU = 1/Rc_resU;
    Rc_resI = 1/Rc_resI;
       
%     fprintf(fres5, '%f', C_resU);
%     fprintf(fres6, '%f', Rc_resU);
%     figure(1);
%     plot(t, ILF);
%     figure(2);
%     plot(t, UnF);

    n = 1;
    for j = 1:2046   
      if(t(j)>2e-4 && t(j) < 4e-4)

           cur_IL = IL(j)/t(j);
           dist_IL = IL(j+1)/t(j+1);
           dist2_IL = IL(j+2)/t(j+2);

           if(n == 1 || n == 3)
               if((dist_IL-cur_IL<0)&&(dist2_IL-dist_IL<0)&&((IL(j+1) - IL(j+2))> 0.04))
%                        fprintf(fres1, 'k = %d', k);
%                        fprintf(fres1, ' IL = %.7f\n', IL(j));
%                        fprintf(fres2, 'k = %d', k);
%                        fprintf(fres2, ' Un = %.7f\n', Un(j));
%                        fprintf(fres3, 'k = %d', k);
%                        fprintf(fres3, ' t = %.8f\n', t(j));
                       if(n == 1)
                           i1 = IL(j);
                           min1_Un = Un(j);
                           t1 = t(j);
                           n = 2;
                       else
                           i3 = IL(j);
                           min2_Un = Un(j);
                           t3 = t(j);
                           n = 4; 
                       end
               end
           end
           if(n == 2)
               if(((dist_IL-cur_IL)>0)&&((dist2_IL-dist_IL)<0)&&(IL(j+2)<IL(j+1)))
%                   fprintf(fres1, 'k = %d', k);
%                   fprintf(fres1, ' IL = %.7f\n', IL(j+1));
%                   fprintf(fres2, 'k = %d', k);
%                   fprintf(fres2, ' Un = %.7f\n', Un(j+1));
%                   fprintf(fres3, 'k = %d', k);
%                   fprintf(fres3, ' t = %.8f\n', t(j+1));
                  i2 = IL(j+1);
                  max_Un = Un(j+1);
                  t2 = t(j+1);
                  n = 3;
               end
           end
           if(n == 4)
               break;
           end 
      end
    end
    %--------------------------------------------------------
    %                       10 om
    %--------------------------------------------------------
    Rn = 0.455 + 9.922;
    Rc = Rc_resU;
    Rl = 0.33;

    U2 = max_Un;
    I = i2;

    di = i1 - i2;
    dt = t1 - t2;

    L1 = (dt/di) * (-I*Rn*Rc/(Rn+Rc) - Rl*I + U2*Rc/(Rc+Rn) - U2 + E);

    U3 = min2_Un;
    I = i3;

    di = i2 - i3;
    dt = t2 - t3;

    L2 = (dt/di) * (-I*Rn*Rc/(Rn+Rc) - Rl*I + U3*Rc/(Rc+Rn) - U3);

    L = (L1+L2)/2;
%     fprintf(fres4, 'L = %f', L);
    
    
    N = 100000;

    I = zeros(1,N);
    U = zeros(1,N);
    Un = zeros(1,N);
    Kf = zeros(1,N);

    f = 14000;
    %-----------------------------------------------------
    Rn = 0.455 + 9.922; %  + 5.034
    %-----------------------------------------------------
    RL = 0.33;
    %-----------------------------------------------------
    C = C_resU;
    %-----------------------------------------------------
    T = 1/f;

    dt = 0.0000001;
    beta = 0.252771100611434;
    Uy = 2.446;
    U0 = 3.08;

    A=[0, 0; 0, 0];

    A(1,1) = (-1/L)*(RL+(Rc*Rn/(Rc+Rn)))*dt;
    A(1,2) = (1/L)*((Rc/(Rc+Rn)) - 1)*dt;
    A(2,1) = (1/C)*(Rn/(Rc+Rn))*dt;
    A(2,2) = -dt/(C*(Rc+Rn));

    A1=[0, 0; 0, 0];

    A1(1,1) = (-1/L)*(RL+(Rc*Rn/(Rc+Rn)));
    A1(1,2) = (1/L)*((Rc/(Rc+Rn)) - 1);
    A1(2,1) = (1/C)*(Rn/(Rc+Rn));
    A1(2,2) = -1/(C*(Rc+Rn));

    X = [0;0];

    B = [E/L;0];

    e_A = expm(A);
    A_ = inv(A1);
    AA = A1\B;
    e_Ax = e_A*X;
    e_AA = e_A*AA;
    EM = eye(2);
    O = EM - e_A;
    OAA = O*AA;
    arr = [];
%     ------------------------ calc -----------------------
    for alpha = 20:70
        t = 0;
        Xr = X;
        flag  = 1;
        I_bif = [];
        for i=1:N
            t = t + dt;

            Up = U0*(t/T - fix(t/T));

            U_n = (X(1)*Rc*Rn/(Rc+Rn)) + X(2)*(1-Rc/(Rc+Rn));
            K_f = alpha * (Uy-beta*U_n) - Up;

            if (t/T - fix(t/T)) <= 0.0014
                I_bif = [I_bif, Xr(1,1)];
                flag = 1;
            end

            if K_f < 0
                flag = 0;
            end

            I(i) = Xr(1,1);
            U(i) = Xr(2,1);
            Un(i) = U_n;

            e_Ax = e_A*X;

            if flag > 0
                Xr = e_Ax + e_AA  - AA;% - OAA;
                Kf(i)=1;
            else
                Xr = e_Ax;
                Kf(i)=0;
            end
            X = Xr;
        end
        arr = [arr; I_bif(end-15:end)];
        
    end
    
%     figure(1);
%     alpha = 20:70;
%     plot(alpha, arr, '.b');
     sub = [1, 51];
    for i = 1:51
        [idx, C] = kmeans(arr(i,:)', 2);
        sub(i) = abs(C(1)-C(2));
    end
    for i = 1:50
        if(sub(i+1)-sub(i)>0.14)
            disp(i);
            disp(sub(i));
            disp(sub(i+1));
            save('name.txt', 'i', '-ascii', '-append');
            break;
        end
    end
end
fclose('all');

%plot_IU;
% 
% UmL = zeros(1,harm_number);
% ImL = zeros(1,harm_number);
% fiL = zeros(1,harm_number);
% ZL = zeros(1,harm_number);
% XL = zeros(1,harm_number);
% RL = zeros(1,harm_number);
% L = zeros(1,harm_number);
% 
% UL = zeros(1,2048);
% 
% for i=1:2047
%     if ILF(i+1)-ILF(i) >= 0
%         UL(i) = UnF(i) - E;
%     else
%         UL(i) = UnF(i);
%     end
% end
% UL(2048) = UL(2047);
% ULF = medfilt1(UL);
% FFT_UL = fft(ILF);
% FFT_IL = fft(ULF);
% 
% for i=1:harm_number
%     
%     num_max_harmI = ceil(freqs(i)/freq_by_harm);     
%     for j=num_max_harmI-2:num_max_harmI+2
%         if (abs(FFT_IL(j)) > abs(FFT_IL(num_max_harmI)))
%             num_max_harmI = j;
%         end
%     end
%     harm_numbers_I(i) = num_max_harmI;
%     
%     num_max_harmU = num_max_harmI;
%     for j=num_max_harmU-2:num_max_harmU+2
%         if (abs(FFT_UL(j)) > abs(FFT_UL(num_max_harmU)))
%             num_max_harmU = j;
%         end
%     end
%     harm_numbers_U(i) = num_max_harmU;
%     
%     ImL(i) = FFT_IL(num_max_harmU);
%     UmL(i) = FFT_UL(num_max_harmI);
%     
%     fiL(i) = angle(UmL(i)) - angle(ImL(i));
%     %while (abs(fiL(i)) > pi)
%      %   fi(i) = abs(fi(i)) -2*pi;
%    % end
%     %if fi(i) < -0.2 && fi(i) > -pi/2
%         ZL(i) = (UmL(i)/ImL(i))*exp(1i*(fiL(i)));
%         XL(i) = abs(ZL(i)*sin(fiL(i)));
%         RL(i) = abs(ZL(i)*cos(fiL(i)));
%         L(i) = XL(i)/(2*pi*freqs(i)); 
%         
%        % C_res = C_res + C(i)*abs(Um(i))/abs(Um(1));
%        % Rc_res = Rc_res + 1/R(i)*abs(Um(i))/abs(Um(1));
%     %end
% 
% end