clc;
clear;

N = 100000;

I = zeros(1,N);
U = zeros(1,N);
Un = zeros(1,N);
Kf = zeros(1,N);

f = 14000;
%-----------------------------------------------------
E = 24;
%-----------------------------------------------------
Rn = 0.455 + 9.922; %  + 5.034
%-----------------------------------------------------
L = 847.77e-06;
RL = 0.33;
%-----------------------------------------------------
C = 60.439e-06;
Rc = 0.25;
%-----------------------------------------------------
T = 1/f;

dt = 0.0000001;
beta = 0.252771100611434;
Uy = 2.446;
U0 = 3.08;

A=[0, 0; 0, 0];

A(1,1) = (-1/L)*(RL+(Rc*Rn/(Rc+Rn)))*dt;
A(1,2) = (1/L)*((Rc/(Rc+Rn)) - 1)*dt;
A(2,1) = (1/C)*(Rn/(Rc+Rn))*dt;
A(2,2) = -dt/(C*(Rc+Rn));

A1=[0, 0; 0, 0];

A1(1,1) = (-1/L)*(RL+(Rc*Rn/(Rc+Rn)));
A1(1,2) = (1/L)*((Rc/(Rc+Rn)) - 1);
A1(2,1) = (1/C)*(Rn/(Rc+Rn));
A1(2,2) = -1/(C*(Rc+Rn));

X = [0;0];

B = [E/L;0];

e_A = expm(A);
A_ = inv(A1);
AA = A1\B;
e_Ax = e_A*X;
e_AA = e_A*AA;
EM = eye(2);
O = EM - e_A;
OAA = O*AA;
arr = [];
for alpha = 20:70
    t = 0;
    Xr = X;
    flag  = 1;
    I_bif = [];
    for i=1:N
        t = t + dt;

        Up = U0*(t/T - fix(t/T));

        U_n = (X(1)*Rc*Rn/(Rc+Rn)) + X(2)*(1-Rc/(Rc+Rn));
        K_f = alpha * (Uy-beta*U_n) - Up;

        if (t/T - fix(t/T)) <= 0.0014
            I_bif = [I_bif, Xr(1,1)];
            flag = 1;
        end

        if K_f < 0
            flag = 0;
        end

        I(i) = Xr(1,1);
        U(i) = Xr(2,1);
        Un(i) = U_n;

        e_Ax = e_A*X;

        if flag > 0
            Xr = e_Ax + e_AA  - AA;% - OAA;
            Kf(i)=1;
        else
            Xr = e_Ax;
            Kf(i)=0;
        end
        X = Xr;
    end
    arr = [arr; I_bif(end-15:end)];
end
figure(1);
alpha = 20:70;
plot(alpha,arr,'.b');